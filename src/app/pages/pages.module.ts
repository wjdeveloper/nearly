import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';

import { PartialsModule } from '../partials/partials.module';

import { PagesComponent } from './pages.component';
import { PostsComponent } from './posts/posts.component';

const pages = [
  PagesComponent,
  PostsComponent
]

@NgModule({
  declarations: pages,
  imports: [
    CommonModule,
    PartialsModule,
    PagesRoutingModule
  ],
  exports: pages
})
export class PagesModule { }
