import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { PostsComponent } from './posts/posts.component';

const routes: Routes = [
    { 
        path: '',
        component: PagesComponent,
        children: [
            { path: '', component: PostsComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule {}
