import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/**
 * Partials
 */
import { HeaderComponent } from './header/header.component';

const partials = [
    HeaderComponent
]

@NgModule({
    declarations: partials,
    imports: [
        CommonModule
    ],
    exports: partials
})
export class PartialsModule {}