import { Injectable, OnInit } from '@angular/core';

import { User } from './user.model';

@Injectable({ providedIn: 'root' })
export class UserService implements OnInit {

    user: User;

    constructor()
    {}
    
    ngOnInit(): void
    {
        const user = new User('wilson antonio', 'juma alcantara', '19860819', 'male', 'lawyer', 'https://scontent.fsti4-2.fna.fbcdn.net/v/t1.0-9/90128472_2520420521552794_319619234101460992_o.jpg?_nc_cat=107&_nc_sid=09cbfe&_nc_eui2=AeFR4RfZfXTHSoGdmA6jH5wj8ErTNlGQwWnwStM2UZDBaeMew932Nkzks34zYgp_7ghH-0NKijIBP-8WNhorraWp&_nc_ohc=pjZcWWaIvAoAX-5G59e&_nc_ht=scontent.fsti4-2.fna&oh=9eb1f8415f45a7677e84091af5426b54&oe=5F9E870C', [], { city: 'sosua', country: 'republica dominicana', street: 'villa liberacion' }, 8296418808, 'wjuma19@gmail.com', 'programmer');

        console.log(user);
    }

}