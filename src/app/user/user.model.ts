

export class User {

    constructor(
        private firstName: string,
        private lastName: string,
        private birth: string,
        private garden: string,
        private role: string,
        private avatar: string,
        private interest: any,
        private address: any,
        private phone: number,
        private email: string,
        private profession: string,

    )
    {
        
    }
}