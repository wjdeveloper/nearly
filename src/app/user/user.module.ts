import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserService } from './user.service';

import { UserComponent } from './user.component';

@NgModule({
    declarations: [ UserComponent ],
    imports: [
        CommonModule
    ],
    exports: [ UserComponent ],
    providers: [
        UserService
    ]
})

export class UserModule {}